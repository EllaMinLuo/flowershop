﻿using System.Collections.Generic;

namespace FlowerShop.Model
{
    public class FlowerShop
    {
        public static List<Flower> Flowers => new List<Flower>()
        {
            new Lily(),
            new Rose(),
            new Lily()
        };
    }
}
