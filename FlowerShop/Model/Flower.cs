﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlowerShop.Model
{
    public abstract class Flower
    {
        public abstract string Code { get; }
        public abstract List<Bundle> Bundles { get; }
    }
}
