﻿using System.Collections.Generic;

namespace FlowerShop.Model
{
    public class Lily : Flower
    {
        private const int BundleOfThreeCount = 3;
        private const int BundleOfSixCount = 6;
        private const int BundleOfNineCount = 9;
        private const double BundleOfThreePrice = 9.95;
        private const double BundleOfSixPrice = 16.95;
        private const double BundleOfNinePrice = 24.95;

        public string Name => "Lilies";
        public override string Code => "L09";

        public static Bundle BundleOfThree => new Bundle()
        {
            Count = BundleOfThreeCount,
            Price = BundleOfThreePrice
        };

        public static Bundle BundleOfSix => new Bundle()
        {
            Count = BundleOfSixCount,
            Price = BundleOfSixPrice
        };

        public static Bundle BundleOfNine => new Bundle()
        {
            Count = BundleOfNineCount,
            Price = BundleOfNinePrice
        };

        public override List<Bundle> Bundles => new List<Bundle>()
        {
            BundleOfNine,
            BundleOfSix,
            BundleOfThree
        };
    }
}
