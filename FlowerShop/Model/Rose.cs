﻿using System.Collections.Generic;

namespace FlowerShop.Model
{
    public class Rose : Flower
    {
        private const int BundleOfFiveCount = 5;
        private const int BundleOfTenCount = 10;
        private const double BundleOfFivePrice = 6.99;
        private const double BundleOfTenPrice = 12.99;

        public static string Name => "Roses";
        public override string Code => "R12";

        public static Bundle BundleOfFive => new Bundle()
        {
            Count = BundleOfFiveCount,
            Price = BundleOfFivePrice
        };

        public static Bundle BundleOfTen => new Bundle()
        {
            Count = BundleOfTenCount,
            Price = BundleOfTenPrice
        };

        public override List<Bundle> Bundles => new List<Bundle>()
        {
            BundleOfTen,
            BundleOfFive
        };
    }
}
