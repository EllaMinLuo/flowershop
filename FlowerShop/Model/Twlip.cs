﻿using System.Collections.Generic;

namespace FlowerShop.Model
{
    public class Twlip : Flower
    {
        private const int BundleOfThreeCount = 3;
        private const int BundleOfFiveCount = 5;
        private const int BundleOfNineCount = 9;
        private const double BundleOfThreePrice = 5.95;
        private const double BundleOfFivePrice = 9.95;
        private const double BundleOfNinePrice = 16.99;

        public string Name => "Twlips";
        public override string Code => "T58";

        public static Bundle BundleOfThree => new Bundle()
        {
            Count = BundleOfThreeCount,
            Price = BundleOfThreePrice
        };

        public static Bundle BundleOfFive => new Bundle()
        {
            Count = BundleOfFiveCount,
            Price = BundleOfFivePrice
        };

        public static Bundle BundleOfNine => new Bundle()
        {
            Count = BundleOfNineCount,
            Price = BundleOfNinePrice
        };

        public override List<Bundle> Bundles => new List<Bundle>()
        {
            BundleOfNine,
            BundleOfFive,
            BundleOfThree
        };
    }
}
