﻿using FlowerShop.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlowerShop
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var orders = Input();
            var resultList = new List<string>();
            foreach (var order in orders)
            {
                var orderDetails = order.Split(' ');
                var orderQuantity = Convert.ToInt32(orderDetails[0]);
                var orderCategory = orderDetails[1];
                var result = Calculate(orderQuantity, orderCategory, order);
                resultList.Add(result);
            }
            Output(resultList);
        }

        public static string Calculate(int orderQuantity, string orderCategory, string order)
        {
            var stringList = new List<string> {order};
            foreach (var flower in Model.FlowerShop.Flowers)
            {
                if (!orderCategory.Equals(flower.Code)) continue;
                double bundlesTotal = 0;
                foreach (var bundle in flower.Bundles)
                {
                    var bundleQuantity = orderQuantity / bundle.Count;
                    orderQuantity = orderQuantity - bundleQuantity * bundle.Count;
                    if (bundleQuantity == 0) continue;
                    var bundleTotal = bundleQuantity * bundle.Price;
                    bundlesTotal = bundlesTotal + bundleTotal;
                    stringList.Add($"{bundleQuantity}x{bundle.Price} {bundleTotal}");
                }
                stringList.Insert(1, $"{bundlesTotal}");
            }

            var result = String.Join("\n", stringList.ToArray());
            return result;
        }

        public static List<string> Input()
        {
            var inputLines = new List<string>();
            Console.WriteLine("Please input your order:");
            while (true)
            {
                var input = Console.ReadLine();
                if (input != null && input.Equals("exit", StringComparison.OrdinalIgnoreCase))
                {
                    break;
                }
                inputLines.Add(input);
            }

            return inputLines;
        }

        public static void Output(List<string> resultList)
        {
            foreach (var result in resultList)
            {
                Console.WriteLine(result);
                Console.ReadLine();
            }
        }
    }
}
